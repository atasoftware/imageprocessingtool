# Basic Image Processing Application for Python / Computer Vision Applications


We have developed a user interface that has capability of apply different type of image processing functions with easily. With a well-designed user interface, different type of processes can be executed.


##  Used  Versions ##

- Python 3.8
- PySide2 5.12.2
- Matplotlib 3.4.2
- OpenCV 4.5.2.52
- Numpy 1.18.0
- PyCharm 2021.1


For now its just support masking operations, but in the next couple of months, different image processing techniques will be added.


## Known Issues ##

- Loading screen can be seen in different resolution.
- Export button works properly only in the RGB color space and will be fixed as soon as possible.
- Contour button is a dummy function in alpha. Functionality of this button will be added in the future.
- Used imports from another files cannot be remembered in the main.py (Ex. Use Main.ui_python)




## Our social media accounts for more information, collaboration etc.: ##

- Emre Anıl Oğuz (https://www.linkedin.com/in/emre-anıl-oğuz-a899aa169/)
- Rabia Elif Çelik (https://www.linkedin.com/in/rabia-elif-çelik-a402a8146/)

